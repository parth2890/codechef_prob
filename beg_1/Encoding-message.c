#include<stdio.h>
#include<string.h>
#include<ctype.h>

void swap(char *,int );
void replace(char *);
int is_lower(char *,int);

int main()
{
    int t;
    scanf("%d",&t);

    while(t--)
    {
        int len;
        scanf("%d",&len);
        char str[len];
        scanf("%s",str);
        
        if(!(is_lower(str,len)))
        {
            printf("lower case only\n");
        }
        else
        {
            swap(str,len);   
        }   
    }
}

int is_lower(char *str,int len)
{
    for(int i =0;i<len;i++){
     
        if(!islower(str[i]))
            return 0;
    }

    return 1;
}

void swap(char *str,int len)
{

    char temp;
    if(len%2==0)
    {
        for(int i=0;i<len-1;i=i+2)
        {
            temp = str[i];
            str[i]= str[i+1];
            str[i+1]= temp;
        }
    }
    else
    {
        for(int i=0;i<len-2;i=i+2)
        {
            temp = str[i];
            str[i] = str[i+1];
            str[i+1] = temp;
        }
    }
    replace(str);
}

void replace(char *str)
{
    int len=strlen(str);
    for(int i=0;i<len;i++)
    {
        str[i] = 'z' + 'a' - str[i];
    }
    printf("%s\n",str);
}