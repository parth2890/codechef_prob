#include<stdio.h>

int solve()
{
    int x,y;
    scanf("%d %d",&x,&y);
    int mul = x*y;
    int ans;

    if(mul>=100)
    {
        ans = (x*y) / 100;
        printf("%d\n",ans);
    }
    else
    {
        printf("0\n");
    }
}

int main()
{
    int t;
    scanf("%d",&t);

    while(t--)
    {
        solve();
    }
    return 0;
}
