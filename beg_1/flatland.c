#include<stdio.h>
int main()
{
    int t;
    scanf("%d",&t);

    while(t--)
    {
        int x;
        scanf("%d",&x);
        int d,count=0;
        
        for(int i=0;i<x;i++)
        {
            d = i*i;
            if(d>x)
            {
                count = d-((i-1)*(i-1));
            }
        }
        int rem;
        rem = x-count;

        int flag=0;
        for(int j=0;j<rem;j++)
        {
            if(j*j > rem)
            {
                flag = (j-1)*(j-1);
            }
            
        }
        printf("%d",flag);
    }
    return 0;
}