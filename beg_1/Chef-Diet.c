#include<stdio.h>

int main()
{
    int t;
    scanf("%d",&t);

    while(t--)
    {
        int n,k;
        scanf("%d %d",&n,&k);

        int arr[n];
        int sum=0;
        int count =0;
        for(int i=0;i<n;i++)
        {
            scanf("%d",&arr[i]);
        }
     
        for(int i=0;i<n;i++)
        {
            sum= sum + arr[i];
            if(sum >= k)
            {
                sum=sum-k;
                count++;    
            }
            else{
                printf("\nNo %d\n",i+1);
                break;
            }
            if(count==n)
            {
                printf("\n Yes\n");
            }
        }
    }
    return 0;
}

