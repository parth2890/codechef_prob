#include<stdio.h>
#include<string.h>

int main()
{
    int t;
    scanf("%d",&t);

    while(t--)
    {
        int n,k;
        scanf("%d %d",&n,&k);

        char s[n];
        scanf("%s",s);

        int count=0;
        int nap=0;
        for(int i=0;i<n;i++)
        {
            if(s[i]=='0')
            {
                count++;
                if(count==k)
                {
                    nap++;
                    count = 0;
                }
            }
            else if(s[i]=='1')
            {
                count = 0;
            }
        }
        printf("%d\n",nap);
    }
    return 0;
}