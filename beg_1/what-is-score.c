#include<stdio.h>

int main()
{
    int t; 
    scanf("%d",&t);

    while(t--)
    {
        int n; 
        scanf("%d",&n);
        int a[11]={0}; 
        int problem,score;
        for(int i=0;i<n;i++)
        {
            scanf("%d",&problem);
            scanf("%d",&score);

            if(problem>0 && problem<=8)
            {
                if(score>a[i])
                {
                    a[i] = score;
                }
            }
        }
        int sum=0;
        for(int i=0;i<9;i++)
        {
            sum=sum+a[i];
        }
        printf("%d\n",sum);
    }
    return 0;
}