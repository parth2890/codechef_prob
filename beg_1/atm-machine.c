#include<stdio.h>

int main()
{
    int t;
    scanf("%d",&t);

    while(t--)
    {
        int n,k;
    
        scanf("%d %d",&n,&k);
        
        int A[n];

        for(int i=0;i<n;i++)
        {
            scanf("%d",&A[i]);
        }
            
        int sum=0;
       
        for(int i=0;i<n;i++)
        {
            if(A[i]>k)
            {
                printf("0\t");
            }
            sum=sum+A[i];
            if(sum>k)
            {
                printf("0\t");
                sum = sum - A[i];
            }
            else
            {
                printf("1\t");
            }
    }

    }
    return 0;
}
