#include<stdio.h>

int main()
{
    int t; //test case
    scanf("%d",&t);

    while(t--)
    {
        int n; //number of movies
        long int x; // disk free user has
        scanf("%d %ld",&n,&x);
        int fit = 0;

        for(int i=0;i<n;i++)
        {
            int s,imdb;
            scanf("%d %d",&s,&imdb);

            if(s<=x && imdb>=fit)
            {
                fit = imdb;
            }
        }
        printf("%d",fit);
    }
    return 0;
}