#include<stdio.h>

void solve(void)
{
    int x;
    scanf("%d",&x);

    if(x%4==0)
    {
        printf("North\n");
    }
    else if(x%4==1)
    {
        printf("East\n");
    }
    else if(x%4==2)
    {
        printf("South\n");
    }
    else
    {
        printf("West\n");
    }
}

int main()
{
    int t;
    scanf("%d",&t);

    while(t--)
    {
        solve();
    }
    return 0;
}
