#include<stdio.h>
#include<string.h>

void solve()
{
    char a[100000];
    scanf("%s",a);
    
    int count_0=0;
    int count_1=0;

    int l = strlen(a);

    for(int i=0;i<l;i++)
    {
        if(a[i]=='1') count_1++;

        else count_0++;
    }

    if(count_1 == 1 || count_0 == 1)
    {
        printf("YES\n");
    }
    else
    {
        printf("NO\n");
    }
}

int main()
{
    int t;
    scanf("%d",&t);

    while(t--)
    {
        solve();
    }
    return 0;
}