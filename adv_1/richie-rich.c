#include<stdio.h>

int solve(void)
{
    int a,b,x;
    scanf("%d %d %d",&a,&b,&x);

    int ans = (b-a)/x;

    printf("%d\n",ans);
}

int main()
{
    int t;
    scanf("%d",&t);

    while(t--)
    {
        solve();
    }
    return 0;
}
