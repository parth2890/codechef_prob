#include<stdio.h>

int solve()
{
    int n,k,v;
    scanf("%d %d %d",&n,&k,&v);
    int sum_k;
    int sum_nk=0;
    int x;
    
    int a[100];
    for(int i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
        sum_nk+=a[i];
    }
    // simple n+k ele's sum - n ele's sum
    // after u got k ele's sum just divide it with k
    // u get ele that are same to each other k times
    sum_k = v*(n+k) - sum_nk;
    x = (sum_k/k);

    // make sure that it is an integer not fraction

    if(sum_k%k==0&&sum_k>0)
        printf("%d\n",x);
    else
        printf("-1\n");
}

int main()
{
    int t;
    scanf("%d",&t);
    while(t--)
        solve();
    return 0;
}