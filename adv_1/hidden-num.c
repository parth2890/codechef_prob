#include<stdio.h>


void solve(void)
{
    long long int n;
    scanf("%lld",&n);

    if(n>=10 && n%2==0)
    {
        n/=2;
        printf("2 %d\n",n);
    }
    else if(n>=3 && n%3==0)
    {
        n/=3;
        printf("3 %d\n",n);
    }
    else
    {
        printf("1 %d\n",n);
    }
}


int main()
{
    int t;
    scanf("%d",&t);

    while(t--)
    {
        solve();
    }
    return 0;
}
