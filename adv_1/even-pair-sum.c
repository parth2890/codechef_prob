#include<stdio.h>

void solve()
{
    long long int a,b;
    scanf("%lld %lld",&a,&b);

    long long int odd_a,odd_b,even_a,even_b;

    if(a%2==0)
    odd_a = even_a = a/2;
    else
    {
        odd_a = (a+1)/2;
        even_a = (a-1)/2;
    }

    if(b%2 == 0)
    odd_b = even_b = b/2;

    else
    {
        odd_b = (b+1)/2;
        even_b = (b-1)/2;
    }

    printf("%lld\n",odd_a*odd_b + even_a*even_b);

}

int main()
{
    int t;
    scanf("%d",&t);
    
    while(t--)
    solve();
    
return 0;
}