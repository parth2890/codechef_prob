#include<stdio.h>

int solve(int a[],int n)
{
    int max_val=a[0];
    for(int i=1;i<n;i++)
    {
        if(a[i]>max_val)
        max_val=a[i];
    }

    int ans = a[0]%max_val;

    for(int i=1;i<n;i++)
    {
        if(a[i]%max_val > ans)
        ans = a[i]%max_val;
    }

    printf("%d\n",ans);

}


int main()
{
    int n;
    scanf("%d",&n);
    int a[n];
    for(int i=0;i<n;i++)
    {
    scanf("%d",&a[i]);
    }
    solve(a,n);
    
}